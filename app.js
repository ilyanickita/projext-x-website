const express = require('express');
const fs = require("fs");

const app = express();
app.use(express.static(__dirname + "/website"));

app.get('/', (req, res) => {
  res.status(200).sendFile(path.join(__dirname + '/website/index.html'));
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});

module.exports = app;
